// socks-hook.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <iostream>
#include <psapi.h>
#include <string>
#include <set>

std::wstring GetProcessNameByID(DWORD processID)
{
    TCHAR szProcessName[MAX_PATH] = TEXT("<unknown>");

    // Get a handle to the process.

    HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION |
        PROCESS_VM_READ,
        FALSE, processID);

    // Get the process name.

    if (NULL != hProcess)
    {
        HMODULE hMod;
        DWORD cbNeeded;

        if (EnumProcessModules(hProcess, &hMod, sizeof(hMod),
            &cbNeeded))
        {
            GetModuleBaseName(hProcess, hMod, szProcessName,
                sizeof(szProcessName) / sizeof(TCHAR));
        }
    }

    // Print the process name and identifier.

    //_tprintf(TEXT("%s  (PID: %u)\n"), szProcessName, processID);

    // Release the handle to the process.

    CloseHandle(hProcess);
    return std::wstring(szProcessName);
}

std::wstring getCurrentProgramPath()
{
    TCHAR result[MAX_PATH];
    return std::wstring(result, GetModuleFileName(NULL, result, MAX_PATH));
}

std::wstring getCurrentProgramDirectory()
{
    auto exePath = getCurrentProgramPath();
    size_t pos = exePath.find_last_of(L"\\");
    return exePath.substr(0, pos + 1);
}

void InjectDLLToProcess(DWORD pid) {
    HANDLE hProcess = OpenProcess(PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION | PROCESS_VM_OPERATION | PROCESS_VM_WRITE | PROCESS_VM_READ,
        FALSE, pid);

    if (hProcess != NULL)
    {
        printf("Process handle: %d, Injecting hook \n", hProcess);
        HANDLE hThread;
        TCHAR  szLibPath[_MAX_PATH] = {0};
        void*  pLibRemote = 0;
        DWORD  hLibModule = 0;

        HMODULE hKernel32 = ::GetModuleHandle(L"Kernel32");

        printf("Kernel32 handle: %d \n", hKernel32);

        //if (!::GetSystemDirectory(szLibPath, _MAX_PATH))
        //    return;

        lstrcatW(szLibPath, (getCurrentProgramDirectory() + std::wstring(L"SocksHookDll.dll")).c_str());
        std::wcout << "DLL Hook path: " << szLibPath << std::endl;

        pLibRemote = ::VirtualAllocEx(hProcess, NULL, sizeof(szLibPath), MEM_COMMIT, PAGE_READWRITE);

        if (pLibRemote == NULL)
            return;

        ::WriteProcessMemory(hProcess, pLibRemote, (void*)szLibPath, sizeof(szLibPath), NULL);

        hThread = ::CreateRemoteThread(hProcess, NULL, 0,
            (LPTHREAD_START_ROUTINE) ::GetProcAddress(hKernel32, "LoadLibraryW"),
            pLibRemote, 0, NULL);

        if (hThread != NULL)
        {
            std::wcout << L"Remote thread created." << std::endl;

            ::WaitForSingleObject(hThread, INFINITE);
            ::GetExitCodeThread(hThread, &hLibModule);
            ::CloseHandle(hThread);

            std::cout << "登录IP修改成功！请不要关闭，多开或重启石器，可以继续使用。" << std::endl;
        }
    }

}


int listProcessAndInsertHook(std::set<DWORD> &hookedSet)
{
    // Get the list of process identifiers.

    DWORD aProcesses[1024], cbNeeded, cProcesses;

    if (!EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded))
    {
        return -1;
    }


    // Calculate how many process identifiers were returned.

    cProcesses = cbNeeded / sizeof(DWORD);

    int count = 0;
    // Print the name and process identifier for each process.
    for (int i = 0; i < cProcesses; i++)
    {
        auto pid = aProcesses[i];
        if (pid != 0)
        {
            auto name = GetProcessNameByID(pid);
            if (name == L"sa_8001.exe" || name == L"test_program.exe")
            {
                if (hookedSet.find(pid) != hookedSet.cend())
                {
                    // Don't inject twice.
                    continue;
                }
                hookedSet.insert(pid);
                std::wcout << "found, pid=" << pid << " name=" << name << std::endl;
                InjectDLLToProcess(pid);
            }
        }
    }
    return count;
}

int main(void)
{

    std::set<DWORD> hookedSet;
    while (true)
    {
        int count = listProcessAndInsertHook(hookedSet);
        if (count == 0 && hookedSet.size() == 0)
        {
            std::cout << "No process named sa_8001.exe or test_program.exe found... 请打开石器，这条信息就会消失。" << std::endl;
            Sleep(2000);
        }
        Sleep(1000);
    }
    system("pause");
    return 0;
}

