// dllmain.cpp : 定义 DLL 应用程序的入口点。
#include "stdafx.h"
#include "detours.h"
#include <iostream>
#include <string>
#include <winsock.h>
#include <fstream>
#include "misc.h"
#include <vector>

PVOID g_pOldMessageBoxW = NULL;
PVOID g_pOldMessageBoxA = NULL;
PVOID g_pOldConnect = NULL;

sockaddr_in g_targetAddr;


typedef int (WINAPI *PfuncMessageBoxA)(HWND hWnd, LPCSTR lpText, LPCSTR lpCaption, UINT uType);
typedef int (WINAPI *PfuncMessageBoxW)(HWND hWnd, LPCWSTR lpText, LPCWSTR lpCaption, UINT uType);

typedef int (WINAPI *PfuncConnect)(SOCKET s, const struct sockaddr *name, int namelen);

////////////  
int WINAPI MyMessageBoxA(HWND hWnd, LPCSTR lpText, LPCSTR lpCaption, UINT uType)
{
    // Just redirect
    return ((PfuncMessageBoxA)g_pOldMessageBoxA)(hWnd, lpText, (std::string("[hacked]") + std::string(lpCaption)).c_str(), uType);
}

////////////  
int WINAPI MyMessageBoxW(HWND hWnd, LPCWSTR lpText, LPCWSTR lpCaption, UINT uType)
{
    // Just redirect
    return ((PfuncMessageBoxW)g_pOldMessageBoxW)(hWnd, lpText, (std::wstring(L"[hacked]") + std::wstring(lpCaption)).c_str(), uType);
}

int WINAPI MyConnect(SOCKET s, const struct sockaddr *name, int namelen)
{
    // The original
    sockaddr_in origAddr = getSockAddr("192.168.1.30", 9065);
    if (namelen == sizeof(sockaddr_in) && memcmp(&origAddr, name, namelen) == 0)
    {
        // Now replace the addr!
        memcpy((void*)name, &g_targetAddr, namelen);
    }
    return ((PfuncConnect)g_pOldConnect)(s, name, namelen);
}


////////////  
BOOL APIENTRY InstallHook()
{
    std::ifstream ipFile(getCurrentProgramDirectory() + L"ip.txt");
    // The default value
    std::string ip = "202.120.61.32";
    uint16_t port = 9065;
    if (ipFile.good())
    {
        std::cout << "reading config from ip.txt" << std::endl;
        ipFile >> ip >> port;
    }
    std::cout << "Set target IP to " << ip << ":" << port << std::endl;
    g_targetAddr = getSockAddr(ip.c_str(), port);

    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    g_pOldMessageBoxA = DetourFindFunction("User32.dll", "MessageBoxA");
    g_pOldMessageBoxW = DetourFindFunction("User32.dll", "MessageBoxW");
    g_pOldConnect = DetourFindFunction("wsock32.dll", "connect");
    DetourAttach(&g_pOldMessageBoxA, MyMessageBoxA);
    DetourAttach(&g_pOldMessageBoxW, MyMessageBoxW);
    DetourAttach(&g_pOldConnect, MyConnect);
    LONG ret = DetourTransactionCommit();
    std::cout << "InstallHook, success = " << (ret == NO_ERROR) << std::endl;
    return ret == NO_ERROR;
}

////////////  
BOOL APIENTRY UnInstallHook()
{
    DetourTransactionBegin();
    DetourUpdateThread(GetCurrentThread());
    DetourDetach(&g_pOldMessageBoxA, MyMessageBoxA);
    DetourDetach(&g_pOldMessageBoxW, MyMessageBoxW);
    DetourDetach(&g_pOldConnect, MyConnect);
    LONG ret = DetourTransactionCommit();
    return ret == NO_ERROR;
}



BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        ::OutputDebugString(L"DLL_PROCESS_ATTACH\n");
        InstallHook();
        break;
    case DLL_THREAD_ATTACH:
        break;
    case DLL_THREAD_DETACH:
        break;
    case DLL_PROCESS_DETACH:
        ::OutputDebugString(L"DLL_PROCESS_DETACH\n");
        UnInstallHook();
        break;
    }
    return TRUE;
}


