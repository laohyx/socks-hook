#pragma once
#include <winsock.h>
#include <string>

static sockaddr_in getSockAddr(const char* ip, uint16_t port) 
{
    struct sockaddr_in serverAddress;
    // Set server address info.
    memset(&serverAddress, 0, sizeof(serverAddress)); // Init.
    serverAddress.sin_family = AF_INET; // Internet address family.
    serverAddress.sin_addr.s_addr = inet_addr(ip); // IP.
    serverAddress.sin_port = htons(port); // Port.
    return serverAddress;
}

EXTERN_C IMAGE_DOS_HEADER __ImageBase;

// Get DLL path
static std::wstring getCurrentProgramPath()
{
    TCHAR result[MAX_PATH];
    return std::wstring(result, GetModuleFileName((HINSTANCE)&__ImageBase, result, MAX_PATH));
}

static std::wstring getCurrentProgramDirectory()
{
    auto exePath = getCurrentProgramPath();
    size_t pos = exePath.find_last_of(L"\\");
    return exePath.substr(0, pos + 1);
}
